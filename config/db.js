const mongoose = require('mongoose');

const connectDb = async () => {
       try {
           await mongoose.connect( process.env.DB_URI , {
               useNewUrlParser: true,
               useUnifiedTopology: true
           });
           console.log("Connected to DB");
           process.emit('appStarted');
       } catch ( err ) {
            console.log(err);
            process.exit(1);
       }
}

module.exports = connectDb;