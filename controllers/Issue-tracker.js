const mongoose = require("mongoose")

const ProjectModel = require("../models/schemas/Project");

const issueTrackerPage = async  (req, res) => {
    const projects = await ProjectModel.find({});
    return res.render('issueTracker', {
        title : 'Issue Tracker',
        addedProject: projects
    })
}

const createProject = (req, res) => {
    return res.render( 'createProject',  {
        title : 'Create Project'
    })
}

const addProjectToDB = async (req , res) => {
    let { projectName , description , authorName} = req.body;
    let project = new ProjectModel({
        projectName,
        projectDescription: description,
        authorName,
    });
    await project.save();
    return res.redirect('/api/issue-tracker');
}

const projectDetails = async (req, res) => {
    let allProjects = await ProjectModel.find({});
    return res.render('projectDetails', {
        title: 'Project Details',
        projectDetails: allProjects
    })
}

const filterProjectDetails = async (req, res) => {
    let { flexRadio } = req.body; 
    let sortBy = { createdAt : -1 };
    if (flexRadio === 'Project Title') {
        sortBy = { projectName : -1 };
    }
    else if (flexRadio === 'Project Description') {
        sortBy = { projectDescription : -1 };
    }
    else if (flexRadio === 'Project Author') {
        sortBy = { authorName : -1 };
    }
    let sortedProjects = await ProjectModel.find({}).sort(sortBy);
    return res.render('projectDetails', { title: "Project Details", projectDetails: sortedProjects })
}

const createAnIssue = (req, res) => {
    const projectId = req.params;
    return res.render('createIssue', {
        title: 'Create Issue',
        projectId
    })
}

const addAnIssue = async (req, res) => {
    let { issueName, issueDescription, authorName} = req.body;
    let projectId = req.params.id;
    let issue = {
        issueName,
        issueDescription,
        authorName,
    }
    let project = await ProjectModel.findByIdAndUpdate(projectId , {$push: {bugs: issue}} , {new: true});
    console.log('project' , project);
    res.redirect('/api/issue-tracker/project-details');
}

module.exports = {
    issueTrackerPage,
    createProject,
    addProjectToDB,
    projectDetails,
    filterProjectDetails,
    createAnIssue,
    addAnIssue
}