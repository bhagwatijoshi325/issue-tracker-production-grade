const { issueTrackerPage, createProject, addProjectToDB, projectDetails, filterProjectDetails, createAnIssue, addAnIssue } = require("../controllers/Issue-tracker");
const { validateSchema } = require("../middlewares/validations/validation");
const { issueValSchema } = require("../middlewares/validations/validation schemas/isuueValidation");
const { projectValSchema, sortProjectSchema } = require("../middlewares/validations/validation schemas/projectValidation");

const router = require("express").Router();

router.get('/' , issueTrackerPage);
router.get('/create-project', createProject);
router.post('/add-project' , validateSchema(projectValSchema), addProjectToDB);
router.get('/project-details', projectDetails);
router.post('/filter-project-details', validateSchema(sortProjectSchema), filterProjectDetails);
router.get('/create-issue/:id', createAnIssue);
router.post('/create-issue/:id/add-issue', validateSchema(issueValSchema), addAnIssue);

module.exports = router;

