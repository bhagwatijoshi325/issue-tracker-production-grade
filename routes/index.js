const router = require("express").Router();
const issueTrackerRouter = require('./issueTracker');

router.get('/hello', (req, res)=> {
    return res.json({
        message: "Hello World"
    });
});

router.use('/api/issue-tracker', issueTrackerRouter )

module.exports = router;