const request  = require("supertest");
const { app, server } = require('../../../server');
const mongoose = require("mongoose");

beforeAll(done => {
    process.on('appStarted', () => {
        done();
    });
});

describe('Issue Tracker API', () => {
    
    // Test for GET /
    it('should display the issue tracker page', async () => {
        const res = await request(app).get('/api/issue-tracker');
        expect(res.statusCode).toEqual(200);
        expect(res.text).toContain('Issue Tracker');
    });

    // Test for GET /create-project
    it('should display the create project page', async () => {
        const res = await request(app).get('/api/issue-tracker/create-project');
        expect(res.statusCode).toEqual(200);
        expect(res.text).toContain('Create Project');
    });

    // Test for POST /add-project
    it('should add a project', async () => {
        const res = await request(app)
            .post('/api/issue-tracker/add-project')
            .send({
                projectName: 'Test Project1',
                description: 'Test Description',
                authorName: 'Test Author'
            });
        expect(res.statusCode).toEqual(302); // assuming you are redirecting
    });

    // ... Add tests for the other routes

});


afterAll(async () => {
    server.close();
    await mongoose.disconnect();
});