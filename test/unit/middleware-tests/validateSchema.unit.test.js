const httpMock = require('node-mocks-http');
const { validateSchema } = require('../../../middlewares/validations/validation');
const Joi = require('joi');

describe('validateSchema middleware', () => {
    it('should proceed to next middleware when data is valid', async () => {
        const mockReq = httpMock.createRequest({
            body: {
                username: 'john',
                password: '12345678'
            }
        });

        const mockResponse = httpMock.createResponse();
        const mockNext = jest.fn();
        const schema = Joi.object({
            username: Joi.string().required(),
            password: Joi.string().required()
        });

        await validateSchema(schema)(mockReq, mockResponse, mockNext);
        expect(mockNext).toHaveBeenCalled();
    });

    it('should return 400 status if data is invalid', async () => {
        const mockRequest = httpMock.createRequest({
            body: { username: '', password: '123456' },
        });
        const mockResponse = httpMock.createResponse();
        const mockNext = jest.fn();
        const schema = Joi.object({
            username: Joi.string().required(),
            password: Joi.string().required(),
        });

        await validateSchema(schema)(mockRequest, mockResponse, mockNext);

        expect(mockResponse.statusCode).toBe(400);
        // console.log('data : ', mockResponse._getData());
        expect(JSON.parse(mockResponse._getData())).toHaveProperty('errors');
    })
})