const httpMock = require('node-mocks-http');

const { issueTrackerPage, createProject, addProjectToDB } = require("../../../controllers/Issue-tracker");


// Mocking ProjectModel.find method
const mockSave = jest.fn();

// Mock static methods
const mockFind = jest.fn();

// Mock ProjectModel class
jest.mock('../../../models/schemas/Project', () => {
  return jest.fn().mockImplementation(() => {
    return { save: mockSave };
  });
});

const ProjectModel = require('../../../models/schemas/Project');

// Explicitly set static method
ProjectModel.find = mockFind;


describe('issueTrackerPage Controller', () => {
    beforeEach( () => {
        jest.clearAllMocks();
    })

    it('should render issueTracker page with projects' , async () =>{
        const req = httpMock.createRequest();
        const res = httpMock.createResponse();

        const mockProject =  [{
            id: 1,
            name: 'project1'
        }, {
            id: 2, 
            name: 'project2'
        }];

        ProjectModel.find.mockResolvedValueOnce(mockProject);

        await issueTrackerPage(req, res);

        expect(ProjectModel.find).toHaveBeenCalledTimes(1);
        expect(res._getRenderView()).toBe('issueTracker');
        expect(res._getRenderData()).toEqual({
            title: 'Issue Tracker',
            addedProject: mockProject
        })

    })

    it('should render the createProject view' , async () => {
        const req = httpMock.createRequest();
        const res = httpMock.createResponse();

        // Call the controller function
        await createProject(req, res);

        // Assertions
        expect(res._getRenderView()).toBe('createProject');
        expect(res._getRenderData()).toEqual({
            title: 'Create Project'
        })
    })

    it('should save the project and redirect', async () => {
        const req = httpMock.createRequest({
            body: {
                projectName: 'New Project',
                description: 'Project Description',
                authorName: 'Author',
            }
        });

        const res = httpMock.createResponse();
        await addProjectToDB(req, res);

        expect(mockSave).toHaveBeenCalledTimes(1);
        expect(res._getRedirectUrl()).toBe('/api/issue-tracker');
    })

})