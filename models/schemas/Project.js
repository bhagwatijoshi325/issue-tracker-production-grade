const mongoose = require('mongoose');

const BugSchema = new mongoose.Schema({
    _id: {
        type: mongoose.Schema.Types.ObjectId,
        default: new mongoose.Types.ObjectId()
    },
    issueName: {
        type: String,
        required: true,
    },
    issueDescription: {
        type: String,
        required: true,
    },
    authorName: {
        type: String,
        required: true
    }
} , {
    timestamps: true
})

const ProjectSchema = new mongoose.Schema({
    projectName: {
        type: String,
        required: true,
    },
    projectDescription: {
        type: String,
        required: true,
    },
    isDeleted: {
        type: Boolean,
        default: false,
    },
    authorName: {
        type: String,
        required: true,
    },
    bugs: {
        type: [
            BugSchema
        ],
        default: []
    }
} , {
    timestamps: true
})

const ProjectModel = mongoose.model('Projects', ProjectSchema);

module.exports = ProjectModel;