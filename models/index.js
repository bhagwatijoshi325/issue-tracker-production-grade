// const mongoose = require('mongoose');
const fs = require('fs');
const path = require("path");

let schemaFiles = fs.readdirSync(path.join(__dirname , 'schemas'))
schemaFiles = schemaFiles.filter( (file) => file.endsWith(".js") );

schemaFiles.forEach( (file) => {
    require(path.join(__dirname , 'schemas' , file));
})