module.exports = {
    testEnvironment: 'node',
    testMatch: [
        "<rootDir>/test/integration/**/*.test.js"
    ],
    // setupFilesAfterEnv: ['./jest.setup.js']
};