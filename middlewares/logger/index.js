const logger = (req, res , next) => {
    console.log("url : " , req.url);
    console.log("method : " , req.method);
    console.log("body : " , req.body);
    console.log("params : " , req.params);
    console.log("query : " , req.query);
    console.log("headers : " , req.headers);
    console.log("cookies : " , req.cookies);
    console.log("signedCookies : " , req.signedCookies);
    console.log("originalUrl : " , req.originalUrl);
    console.log('------------------------------------------');
    next()
}

module.exports = {
    logger: logger
}