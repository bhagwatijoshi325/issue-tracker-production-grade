const Joi = require("joi");

// custom validation middleware
const validateSchema = (schema) => {
    return async (req, res, next) => {
        try {
            const result = await schema.validateAsync(req.body, {
                abortEarly: false,
                convert: true,
            });
            req.body = result;
            next();
        } catch (err) {
            const errorDetails = err.details.map((details) => {
                if (details.context?.message){
                    return details.context.message; 
                } else {
                    return details.message;
                }
            });
            return res.status(400).json({
                errors: errorDetails,
            });
        }
    }
}

module.exports = {
    validateSchema
};