const Joi = require("joi");
const ProjectModel = require("../../../models/schemas/Project");

const projectValSchema =  Joi.object({
    projectName: Joi.string().trim().strict().custom( removeMiddleSpace).min(1).max(100).required().external(uniqueProjectName),
    description: Joi.string().trim().strict().custom( removeMiddleSpace).min(1).max(100).required(),
    authorName: Joi.string().trim().strict().custom( removeMiddleSpace).min(1).max(100).required(),
})

function removeMiddleSpace (value, helpers) {
    return value.replace(/\s+/g, ' ');
}

async function uniqueProjectName (value , helpers) {
    try {
        console.log('value : ', value);
        const isUnique = await ProjectModel.findOne({
            projectName: value,
            isDeleted: false
        })
        console.log('isUnique : ', isUnique);
        if (isUnique) {
            return helpers.error("any.custom", {
                message: "ProjectName already exists"
            })
        }
        return value;
    } catch (err) {
        throw err;  
    }
}

// filter joi schema
const sortProjectSchema = Joi.object({
    flexRadio: Joi.string().valid('Project Title', 'Project Description', 'Project Author').required(),
})

module.exports = {
    projectValSchema,
    sortProjectSchema
}