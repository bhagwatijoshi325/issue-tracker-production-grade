const Joi = require("joi");

const issueValSchema =  Joi.object({
    issueName: Joi.string().trim().strict().custom( removeMiddleSpace).min(1).max(100).required(),
    issueDescription: Joi.string().trim().strict().custom( removeMiddleSpace).min(1).max(100).required(),
    authorName: Joi.string().trim().strict().custom( removeMiddleSpace).min(1).max(100).required(),
})

function removeMiddleSpace (value, helpers) {
    return value.replace(/\s+/g, ' ');
}

module.exports = {
    issueValSchema
}