// imports
console.log( "ENV : ",  process.env.ENV);
if(process.env.ENV == 'dev'){
    require('dotenv').config({ path: '.env'});
} else if (process.env.ENV == 'test'){
    require('dotenv').config({ path: '.env.test'});
} else if (process.env.ENV == 'prod'){
    require('dotenv').config({ path: '.env.prod'});
} else {
    throw new Error("!!! Unknown Environment")
}
if (!process.env.EXISTS){
    throw new Error("!!! Unknown Environment");
}

const express = require("express");
const expressLayouts = require('express-ejs-layouts');
const router = require("./routes");
const { logger } = require("./middlewares/logger");
const path = require('path');
const helmet = require("helmet");
const cors = require("cors");
const rateLimit = require("express-rate-limit");

require('./config/db')();

// create app
const app = express();


// Security Measures
// app.use(helmet());
app.use(cors());
// app.use(rateLimit({ w4indowMs: 1 * 60 * 1000, max: 100 })); // 100 request in 1 second


app.use(express.urlencoded({extended: true})); // helps to parse the x-www-form-urlencoded type body data
app.use(express.json()); // helps to parse json body type payload
app.use(expressLayouts); // so that we can use include header, footer and body in the ejs, and later on extract styles and scripts are dependent on this
app.set('layout extractStyles', true); // extract all styles from sub layouts at 1 place
app.set('layout extractScripts', true); // extract all the scripts at 1 place

app.set('view engine' , 'ejs'); // set template engine

app.set('views' , path.join(__dirname , 'views')); // set template directory

app.use(express.static('./assets')); // directory for html and css files

// apply global middlewares
app.use(logger);

// add routers
app.use(router);

// listen app on the port
let server = app.listen(process.env.SERVER_PORT , ()=> {
    console.log(`Server is listening at ${process.env.SERVER_PORT}`)
});

module.exports = {
    app : app,
    server
}